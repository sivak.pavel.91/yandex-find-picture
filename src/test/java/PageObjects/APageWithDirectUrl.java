package PageObjects;

import com.codeborne.selenide.Selenide;

//Подозреваю, что не правильно тут реализовано всё наследование
public abstract class APageWithDirectUrl extends APage {
    String url;

    protected APageWithDirectUrl(String url, String name) {
        super(name);
        this.url = url;
    }

    public void open() {
        Selenide.open(url);
    }

}
