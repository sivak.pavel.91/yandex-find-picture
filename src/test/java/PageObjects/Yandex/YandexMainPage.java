package PageObjects.Yandex;

import PageObjects.APageWithDirectUrl;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class YandexMainPage extends APageWithDirectUrl {

    private final SelenideElement picturesButton = $(By.xpath("//a[@data-id='images']"));

    public YandexMainPage() {
        super("http://www.yandex.ru", "Главная страница яндекса");
    }

    public void clickPicturesButton() {
        this.click(picturesButton);
    }

}
