package PageObjects.Yandex;

import PageObjects.APageWithDirectUrl;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;

public class YandexPicturesPage extends APageWithDirectUrl {

    //Не красивые Xpath какие-то..
    private final SelenideElement findByPicturesButton = $(By.xpath("//span[@class='icon icon_type_cbir input__button']"));
    private final SelenideElement chooseFileButton = $(By.xpath("//input[@class='cbir-panel__file-input']"));

    public YandexPicturesPage() {
        super("https://yandex.ru/images/", "Яндекс - картинки");
    }

    public void clickFindByPicturesButton() {
        findByPicturesButton.click();
    }

    public void uploadFile(File file) {
        chooseFileButton.uploadFile(file);
    }
}
