package PageObjects.Yandex;

import PageObjects.APage;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;


import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class YandexResultFindPicturePage extends APage {

    private final ElementsCollection seemsBlockElements = $$(By.xpath("//div[@class='Tags Tags_type_simple']/a"));
    private final SelenideElement seemsBlock = $(By.xpath("//div[@class='CbirItem CbirTags']"));

    public YandexResultFindPicturePage() {
        super("Результат поиска по картинкам");
    }

    public ElementsCollection getSeemsBlockElements() {
        //Надо реализовать полноценный waitForPageLoad, а не ждать каждый "опасный" элемент. В текущей архитектуре сходу красиво не сделал
        this.waitForElementLoad(seemsBlock);
        return seemsBlockElements;
    }

}
