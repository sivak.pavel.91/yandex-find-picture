package PageObjects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;

public abstract class APage implements IPage {
    @Getter
    String name;

    protected APage(String name) {
        this.name = name;
    }

    public void waitForElementLoad(SelenideElement element) {
                /*  Почему-то блок с элементами управления главной страницы яндекса (где "картинки"), загружается не всегда.
        Для таких случаев нужно count раз попробовать перезагрузить страницу  */
        element.shouldBe(Condition.visible);
    }

    public void click(SelenideElement element) {
        waitForElementLoad(element);
        element.click();
    }
}
