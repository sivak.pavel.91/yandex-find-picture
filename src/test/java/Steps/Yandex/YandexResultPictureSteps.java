package Steps.Yandex;

import PageObjects.Yandex.YandexResultFindPicturePage;
import Steps.ASteps;
import lombok.extern.java.Log;

import static org.junit.Assert.*;


@Log
public class YandexResultPictureSteps extends ASteps<YandexResultFindPicturePage> {

    public YandexResultPictureSteps() {
        super(new YandexResultFindPicturePage());
    }

    public void checkKeyWordInSeemsBlock(String keyWord) {
        assertTrue(String.format("Слово '%s' не найдено в блоке 'Кажется на картинке есть'", keyWord),
                page.getSeemsBlockElements().stream().anyMatch(e -> e.getText().contains(keyWord)));
        log.info(String.format("Слово '%s' найдено в блоке 'Кажется на картинке есть'", keyWord));
    }
}
