package Steps.Yandex;

import PageObjects.Yandex.YandexMainPage;
import Steps.ASteps;
import lombok.extern.java.Log;

import static com.codeborne.selenide.Selenide.switchTo;

@Log
public class YandexMainPageSteps extends ASteps<YandexMainPage> {

    public YandexMainPageSteps() {
        super(new YandexMainPage());
    }

    public void goToPictures() {
        log.info("Осуществляется переход в картинки");
        page.clickPicturesButton();
        //Не очень мне нравится такой свич, по другому сходу не придумал
        switchTo().window("Яндекс.Картинки: поиск изображений в интернете, поиск по картинке");
    }
}
