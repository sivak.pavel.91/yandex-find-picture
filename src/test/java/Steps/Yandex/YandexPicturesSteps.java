package Steps.Yandex;

import PageObjects.Yandex.YandexPicturesPage;
import Steps.ASteps;
import lombok.extern.java.Log;

import java.io.File;

@Log
public class YandexPicturesSteps extends ASteps<YandexPicturesPage> {

    public YandexPicturesSteps() {
        super(new YandexPicturesPage());
    }

    public void uploadPictureForFind(String fileName) {
        log.info(String.format("Осуществляется загрузка файла для поиска %s", fileName));
        //Надо путь до ресурсов зафиксировать где-то глобально
        File file = new File("src\\test\\resources\\Pictures\\" + fileName);
        page.clickFindByPicturesButton();
        page.uploadFile(file);
    }
}
