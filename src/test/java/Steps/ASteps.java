package Steps;

import Exeptions.PageCannotOpenByUrl;
import PageObjects.APageWithDirectUrl;
import PageObjects.IPage;
import lombok.SneakyThrows;
import lombok.extern.java.Log;

@Log
//Перемудрил?
public abstract class ASteps<P extends IPage> {
    protected P page;

    protected ASteps(P page) {
        this.page = page;
    }

    @SneakyThrows
    public void openPageByUrl() {
        if (page instanceof APageWithDirectUrl) {
            log.info(String.format("Открывается страница %s", page.getName()));
            ((APageWithDirectUrl) page).open();
        } else {
            throw new PageCannotOpenByUrl(String.format("Страницу %s нельзя открыть по урлу", page.getName()));
        }
    }
}
