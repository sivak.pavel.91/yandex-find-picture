package Exeptions;

public class PageCannotOpenByUrl extends Exception {

    public PageCannotOpenByUrl(String message) {
        super(message);
    }
}
