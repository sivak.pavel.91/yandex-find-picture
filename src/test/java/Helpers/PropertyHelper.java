package Helpers;

import lombok.SneakyThrows;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyHelper {
    static Properties applicationProperty;

    @SneakyThrows
    public static String getApplicationPropertyValue(String key) {
        if (applicationProperty == null) {
            applicationProperty = new Properties();
            applicationProperty.load(new FileInputStream("application.properties"));
        }
        return applicationProperty.getProperty(key);
    }
}
