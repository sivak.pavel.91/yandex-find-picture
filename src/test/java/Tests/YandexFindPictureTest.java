package Tests;

import Steps.Yandex.YandexMainPageSteps;
import Steps.Yandex.YandexPicturesSteps;
import Steps.Yandex.YandexResultPictureSteps;
import com.codeborne.selenide.Configuration;
import lombok.SneakyThrows;
import org.junit.BeforeClass;
import org.junit.Test;

import static Helpers.PropertyHelper.getApplicationPropertyValue;


public class YandexFindPictureTest {

    @BeforeClass
    public static void setDriver() {
        Configuration.browser = getApplicationPropertyValue("browser");
    }

    @Test
    @SneakyThrows
    public void checkSeemsKeyWord() {
        //Думаю, что эти импорты можно как-то красиво заинжектить
        YandexMainPageSteps mainPageSteps = new YandexMainPageSteps();
        YandexPicturesSteps picturesSteps = new YandexPicturesSteps();
        YandexResultPictureSteps resultPictureSteps = new YandexResultPictureSteps();

        mainPageSteps.openPageByUrl();
        mainPageSteps.goToPictures();
        picturesSteps.uploadPictureForFind("cats.jpg");
        resultPictureSteps.checkKeyWordInSeemsBlock("котята");
    }
}
